#!/usr/bin/env python3
import serial
import time
from signal import signal, SIGTERM, SIGHUP, pause
from rpi_lcd import LCD
import numpy as np
import cv2
from picamera import PiCamera
from time import sleep

#Open and start serial port for the Arduino
ser = serial.Serial('/dev/ttyACM0', 115200, timeout=1)
ser.flush()

##Open and start serial port for the STM32
ser1 = serial.Serial('/dev/ttyACM1', 115200, timeout=1)
ser1.flush()

#Initialize LCD
lcd = LCD()

#Initialize Camera
camera = PiCamera()

while True:
    #Start of Temperature, Humidity, and Light readings from Arduino#
    
    #Grab reading from arduino
    def arduino_data() :
        if ser.in_waiting > 0:
            recieved_data = ser.readline().decode('utf-8').rstrip()
            time.sleep(0.3)
            
            #Split the data up into terms for temp, humid, light
            data_split = recieved_data.split('.')
            temp = data_split[0]
            humid = data_split[1]
            light = data_split[2]
            
            #Display these values to the LCD
            lcd.clear()
            lcd.text(temp + humid,1)
            lcd.text(light,2)
            time.sleep(5)
            ser.close()
            ser.open()
        else:
            time.sleep(0.3)
            arduino_data()

    arduino_data()
        
    #Start of EC, pH, and Water Level

    #Grab reading from stm
    def stm_data() :
        if ser1.in_waiting > 0:
            recieved_data1 = ser1.readline().decode('utf-8').rstrip()
            time.sleep(0.3)
            
            #Split the data up into terms for temp, humid, light
            data_split1 = recieved_data1.split('/')
            water_level = data_split1[0]
            water_distance = data_split1[1]
            EC = data_split1[2]
            pH = data_split1[3]
            
            #Display these values to the LCD
            lcd.clear()
            lcd.text(water_level,1)
            lcd.text(water_distance,2)
            time.sleep(5)
            lcd.text(EC,1)
            lcd.text(pH,2)
            time.sleep(1.5)
            ser1.close()
            ser1.open()
        else:
            time.sleep(0.3)
            stm_data()
    stm_data()
        
    #Start of Camera#
    
    #Take picture
    camera.capture('/home/pi/picture.jpg')  
    
    #Read the image and store the hsv range into variable
    img_original =  cv2.imread('picture.jpg')
    
    
    # Cropping the image
    img = img_original[0:1080, 350:1500]

    
    #Convert the resolution of the image
    img = cv2.resize(img, (1280, 720))  

    #Convert from rgb to hsv
    hsv = cv2.cvtColor(img, cv2.COLOR_BGR2HSV)
    
    #Find the nitrogen deficiencies hsv range
    mask_nitrogen_inside1 = cv2.inRange(hsv, (136,0,238), (179,19,255))#Inside1
    mask_nitrogen_inside2 = cv2.inRange(hsv, (144,16,201), (173,35,248))#Inside2
    mask_nitrogen_outside1 = cv2.inRange(hsv, (0,0,0), (0,0,255)) #Outside1
    mask_nitrogen_outside2 = cv2.inRange(hsv, (29,20,0), (33,255,255)) #Outside2
    mask_nitrogen_inside = cv2.bitwise_or(mask_nitrogen_inside1, mask_nitrogen_inside2)
    mask_nitrogen_outside = cv2.bitwise_or(mask_nitrogen_outside1, mask_nitrogen_outside2)
    
    #Find the potassium deficiencies hsv range
    mask_potassium_inside = cv2.inRange(hsv, (161, 20, 186), (179, 38, 226))#Inside1
    mask_potassium_outside = cv2.inRange(hsv, (16, 22, 227), (40, 69, 247))#Outside1
    
    #Find the phosporus deficiencies hsv range
    mask_phosphorus_inside = cv2.inRange(hsv, (125, 22, 180), (139, 64, 220))#Inside1
    mask_phosphorus_outside = cv2.inRange(hsv, (40, 44, 141), (69, 67, 240))#Outside1
    
    #Bitwise-AND nitrogen and original image
    res_nitrogen_inside = cv2.bitwise_and(img,img, mask= mask_nitrogen_inside)
    res_nitrogen_outside = cv2.bitwise_and(img,img, mask= mask_nitrogen_outside)
    
    #Bitwise-AND potassium and original image
    res_potassium_inside = cv2.bitwise_and(img,img, mask= mask_potassium_inside)
    res_potassium_outside = cv2.bitwise_and(img,img, mask= mask_potassium_outside)
    
    #Bitwise-AND phosphorus and original image
    res_phosphorus_inside = cv2.bitwise_and(img,img, mask= mask_phosphorus_inside)
    res_phosphorus_outside = cv2.bitwise_and(img,img, mask= mask_phosphorus_outside)
    
    #Convert the resolution of the image
    res_nitrogen_inside = cv2.resize(res_nitrogen_inside, (1280, 720))
    res_nitrogen_outside = cv2.resize(res_nitrogen_outside, (1280, 720))
    res_potassium_inside = cv2.resize(res_potassium_inside, (1280, 720))
    res_potassium_outside = cv2.resize(res_potassium_outside, (1280, 720))
    res_phosphorus_inside = cv2.resize(res_phosphorus_inside, (1280, 720))
    res_phosphorus_outside = cv2.resize(res_phosphorus_outside, (1280, 720))
    img = cv2.resize(img, (1280, 720))  
    
    #Display the images of deficiencies as well as the original
    cv2.imshow("original", img)
    cv2.imshow("Nitrogen Deficiencies Inside", res_nitrogen_inside)
    cv2.imshow("Nitrogen Deficiencies Outside", res_nitrogen_outside)
    cv2.imshow("Potassium Deficiencies Inside", res_potassium_inside)
    cv2.imshow("Potassium Deficiencies Outside", res_potassium_outside)
    cv2.imshow("Phosphorus Deficiencies Inside", res_phosphorus_inside)
    cv2.imshow("Phosphorus Deficiencies Outside", res_phosphorus_outside)
    
    #Count and Store the pixels of each deficient image to quantify
    nitrogen_pixles_inside = cv2.countNonZero(mask_nitrogen_inside)
    nitrogen_pixles_outside = cv2.countNonZero(mask_nitrogen_outside)
    potassium_pixles_inside = cv2.countNonZero(mask_potassium_inside)
    potassium_pixles_outside = cv2.countNonZero(mask_potassium_outside)
    phosphorus_pixles_inside = cv2.countNonZero(mask_phosphorus_inside)
    phosphorus_pixles_outside = cv2.countNonZero(mask_phosphorus_outside)
    
    # #Print results of the quantified deficiency debug
    # print('The number of Nitrogen Deficient pixels inside is: ' + str(nitrogen_pixles_inside))
    # print('The number of Nitrogen Deficient pixels outside is: ' + str(nitrogen_pixles_outside))
    # print('The number of Potassium Deficient pixels inside is: ' + str(potassium_pixles_inside))
    # print('The number of Potassium Deficient pixels outside is: ' + str(potassium_pixles_outside))
    # print('The number of Phosphorus Deficient pixels inside is: ' + str(phosphorus_pixles_inside))
    # print('The number of Phosphorus Deficient pixels outside is: ' + str(phosphorus_pixles_outside))
    
    #Insert code to print to lcd
    d = 0 #Deficiency counter
    if ((nitrogen_pixles_outside > 11000) or (nitrogen_pixles_inside > 11000)):
        lcd.clear()
        lcd.text('Nitrogen',1)
        lcd.text('deficiency!',2)
        d=d+1
        time.sleep(5)
    if (potassium_pixles_outside > 8000 or (potassium_pixles_inside > 1790 and potassium_pixles_inside > (nitrogen_pixles_inside/7))):
        lcd.clear()
        lcd.text('Potassium',1)
        lcd.text('deficiency!',2)
        d=d+1
        time.sleep(5)
    if ( (phosphorus_pixles_outside > 19900 and phosphorus_pixles_outside > potassium_pixles_outside*4) or (phosphorus_pixles_inside > 17000 and phosphorus_pixles_inside > nitrogen_pixles_inside + 9000 and phosphorus_pixles_inside > potassium_pixles_inside*14) ):
        lcd.clear()
        lcd.text('Phosphorus',1)
        lcd.text('deficiency!',2)
        d=d+1
        time.sleep(5)
    if (d < 1 ):
        lcd.clear()
        lcd.text('Plant Healthy',1)
        time.sleep(5)
        
    #cv2.waitKey(0)
    cv2.destroyAllWindows()